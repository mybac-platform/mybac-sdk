# MyBac Content

This codebase contains the following features:

-   JSON Schemas for validating _Frame_ contents and how to solve them.
-   Client-side code for rendering _Content Blocks_ into pure HTML (i.e. the `MyBac.render()` global function).

## What is a "Frame"?

MyBac content is organized in _Courses_, _Campaigns_, _Missions_. A _Mission_ is an ordered succession of **_Frames_** where each _Frame_ is one single step to be displayed to the user and played.

All _Frame_ objects contain the `Frame.cover` field which is the main Content Block for that _Frame_. Depending on the `Frame.type` there can be other Content Blocks in the _Frame_ structure as well (e.g. see the `challenge.multiple-choice` frame type).

See the [./FRAMES.md](./FRAMES.md) file for currently implemented frame types and details about their internal structure.

## What is a "Content Block"?

Content Blocks are basically MyBac content data encoded as plain JSON objects which can be rendered to plain HTML using the `MyBac.render()` function. The resulting HTML code can be used directly in your app's UI (and also styled additionally via CSS).

You don't have to worry about the internal structure of Content Blocks, but FYI: each Content Block object contains the `type` field (example values `article`, `video`, `map` etc.) and a `data` field; both are converted by `MyBac.render()` into pure HTML.

## How to render Content Bocks?

1. Clone this repository and build the output files.

    ```sh
    yarn install
    yarn build
    ```

    The generated files will be placed in the `./dist` folder.

2. Load the generated `mybac-sdk.css` and `mybac-sdk.js` files in your app, preferably in the `<head></head>` section of your HTML code.

    ```html
    <head>
        ...
        <link rel="stylesheet" type="text/css" href=".../dist/mybac-sdk.css" />
        <script src=".../dist/mybac-sdk.js"></script>
        ...
    </head>
    ```

This makes the `MyBac` namespace available globally in the host `window`.

3. Render content blocks into HTML and place them inside your DOM.

    ```html
    <div id="my-target"></div>
    <script>
        const target = document.getElementById("my-target");

        target.innerHTML = MyBac.render({
            /* ...Content Block JSON... */
        });
    </script>
    ```

## Notes

### Customizing CSS

To customize the included CSS styles, just overwrite them by appending additional rules in your app. All the base CSS rules are children of the `.mybac` class and the rules are minimal.

### Shadow DOM

If you want to make sure the content's styles are completely isolated from your app's CSS styles, you can put the rendered HTML inside a Shadow DOM, like so:

```html
<div id="my-target" style="all: initial;"></div>
<script>
    const target = document.getElementById("my-target");
    const shadow = target.attachShadow({ mode: "open" });

    const content = MyBac.render({
        /* ...Content Block JSON... */
    });

    shadow.innerHTML = `
        <link rel="stylesheet" type="text/css" href=".../mybac-sdk.css">
        <!-- Add custom styles here if you want. -->
        ${content}
    `;
</script>
```

See [here](https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_shadow_DOM) for more about Shadow DOM's and how they work.
