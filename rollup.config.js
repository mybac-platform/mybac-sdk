import babel from "@rollup/plugin-babel";
import commonjs from "@rollup/plugin-commonjs";
import copy from "rollup-plugin-copy";
import resolve from "@rollup/plugin-node-resolve";
import scss from "rollup-plugin-scss";
import terser from "rollup-plugin-terser";
import typescript from "@rollup/plugin-typescript";

export default {
    input: "src/ts/index.ts",
    output: {
        file: "dist/mybac-sdk.js",
        format: "iife",
        name: "MyBac",
        // format: "es",
        exports: "default",
        sourcemap: true,
    },
    plugins: [
        scss({
            outputStyle: "compressed",
        }),
        copy({
            targets: [{ src: "node_modules/katex/dist/fonts/*", dest: "dist/fonts" }],
        }),
        resolve({
            browser: true,
        }),
        commonjs(),
        babel({
            compact: true,
            babelHelpers: "bundled",
        }),
        terser.terser({
            format: {
                ascii_only: true,
            },
        }),
        typescript(),
    ],
    onwarn(message) {
        // Circular dependencies are valid in ES6.
        if (message.code === "CIRCULAR_DEPENDENCY") {
            return;
        }
        console.warn(message);
    },
};
