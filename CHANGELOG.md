# MyBac Content

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 2022-03-23

-   Added Rooms API.
-   Ported to TypeScript.

## 2022-01-05

-   Schemas:
    -   Added schemas for content blocks (article and video), referenced from frame schemas.

## 2022-01-03

-   First commit into repository.
-   Base JSON Schema files.
-   Base rendering code.
