export default interface RendererFunction {
    (data: any, preview: boolean): string;
}
