import article from "./blocks/article";
import video from "./blocks/video";
import IRendererFunction from "./contracts/IRendererFunction";

/**
 * Supported EditorJS block renderers.
 */
const renderers: { [key: string]: IRendererFunction } = {
    article,
    video,
};

export type TContent = {
    type: string;
    data: any;
};

/**
 * Parse and render an EditorJS JSON document as HTML.
 * The result is HTML code returned as a string.
 *
 * @param {JSON} content The content data definition.
 * @param {boolean} preview If `true` the result will be optimized for preview only, i.e. no interaction or other features required for play.
 * @returns {string} The generated HTML as a string.
 */
export default function (content: TContent, preview: boolean = false): string {
    const { type, data } = content;

    return renderers[type]
        ? `<div class="mybac__block">${renderers[type](data, preview)}</div>`
        : `<div class="mybac__block"><pre>${data}</pre></div>`;
}
