import IRendererFunction from "../contracts/IRendererFunction";

export type TVideoData = {
    poster: string;
    source: string;
    outputs: TVideoDataOutput[];
};

export type TVideoDataOutput = {
    type?: string;
    url: string;
};

/**
 * Parse and render a video definition.
 *
 * @param {JSON} data The content data object.
 * @param {boolean} preview If `true`, disabled interactive features (where possible).
 * @returns HTML content as a string.
 */
const renderer: IRendererFunction = function (data: TVideoData, preview: boolean = false) {
    const { poster = "", source } = data;
    const { outputs = [{ url: source }] } = data;

    const attributes = preview ? `preload="none"` : `preload="auto" controls autoplay`;

    const subparts = outputs.map((output) => {
        const { type, url } = output;
        return `<source src="${url}" type="${type}" />`;
    });

    return `<video controlslist="nodownload" poster="${poster}" ${attributes}>${subparts.join("")}</video>`;
};

export default renderer;
