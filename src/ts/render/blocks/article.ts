import katex from "katex";

import "katex/contrib/mhchem/mhchem.js";
import IRendererFunction from "../contracts/IRendererFunction";

export type TArticleData = {
    time: number;
    blocks: TArticleDataBlock[];
};

export type TArticleDataBlock = {
    id?: string;
    type: string;
    data:
        | TArticleDataParagraph
        | TArticleDataHeader
        | TArticleDataList
        | TArticleDataTable
        | TArticleDataImage
        | TArticleDataDelimiter
        | TArticleDataKatex;
};

export type TArticleDataParagraph = {
    text: string;
};

export type TArticleDataHeader = {
    level: number;
    text: string;
};

export type TArticleDataList = {
    style: string;
    items: string[];
};

export type TArticleDataTable = {
    withHeadings: boolean;
    content: string[][];
};

export type TArticleDataDelimiter = {};

export type TArticleDataImage = {
    caption: string;
    file: {
        url: string;
    };
    stretched: boolean;
    withBorder: boolean;
    withBackground: boolean;
};

export type TArticleDataKatex = {
    latex: string;
};

/**
 * Supported EditorJS block renderers.
 */
const renderers: { [key: string]: IRendererFunction } = {
    paragraph: function (data: TArticleDataParagraph, preview: boolean) {
        return `<p>${data.text}</p>`;
    },

    header: function (data: TArticleDataHeader, preview: boolean) {
        return `<h${data.level}>${data.text}</h${data.level}>`;
    },

    list: function (data: TArticleDataList, preview: boolean) {
        const type = data.style === "ordered" ? "ol" : "ul";
        const items = data.items.reduce((result, item) => result + `<li>${item}</li>`, "");
        return `<${type}>${items}</${type}>`;
    },

    table: function (data: TArticleDataTable, preview: boolean) {
        const parts = [];
        parts.push(`<table>`);

        if (data.withHeadings) {
            const row = data.content.shift() || [];
            const cells = row.map((cell) => `<th>${cell}</th>`);
            parts.push(`<thead><tr>${cells.join("")}</tr></thead>`);
        }

        const rows = data.content.map((row) => {
            const cells = row.map((cell) => `<td>${cell}</td>`);
            return `<tr>${cells.join("")}</tr>`;
        });
        parts.push(`<tbody>${rows.join("")}</tbody>`);

        parts.push("</table>");

        return parts.join("");
    },

    image: function (data: TArticleDataImage, preview: boolean) {
        const styles = [
            data.stretched ? "image--stretched" : "",
            data.withBorder ? "image--with-border" : "",
            data.withBackground ? "image--with-background" : "",
        ].join(" ");
        return `<figure class="${styles}"><img src="${data.file.url}" alt="${data.caption}"><figcaption>${data.caption}</figcaption></figure>`;
    },

    delimiter: function (data: TArticleDataDelimiter, preview: boolean) {
        return `<hr />`;
    },

    katex: function (data: TArticleDataKatex, preview: boolean) {
        return katex.renderToString(data.latex, {
            output: "html",
            throwOnError: false,
            displayMode: true,
        });
    },
};

/**
 * Parse and render an EditorJS JSON document.
 *
 * @param {JSON} data The content data object.
 * @param {boolean} preview If `true`, disabled interactive features (where possible).
 * @returns HTML content as a string.
 */
const renderer: IRendererFunction = function (data: TArticleData, preview: boolean = false) {
    const blocks = data.blocks || [];
    const pieces = blocks.map((block) => {
        const type = block.type;

        if (!renderers[type]) {
            return `<div class="error">rendering '${type}' block type is not supported</div>`;
        }

        try {
            return renderers[type](block.data, preview);
        } catch (error) {
            console.error(error);
            return `<div class="error">rendering '${type}' block failed (see console for error)</div>`;
        }
    });
    const html = pieces.join("");
    return `<article>${html}</article>`;
};

export default renderer;
