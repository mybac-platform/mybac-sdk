import render from "./render/index";
import rooms from "./rooms/index";

/**
 * Compile SCSS styles.
 */
import "../scss/index.scss";

/**
 * Static SDK exports.
 */
export default {
    render,
    rooms,
};
