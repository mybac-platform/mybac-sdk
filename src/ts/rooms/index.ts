import adapter from "webrtc-adapter";
import { connect, disconnect, socket } from "./socket";
import { on, off, offAll } from "./events";

// Make sure the shim is loaded.
adapter.browserDetails.version;

const state: {
    room?: string;
    muted?: boolean;
} = {};

async function call(command: string, response: string, data: any = {}) {
    const _id = Date.now() + Math.floor(Number.MAX_SAFE_INTEGER * Math.random());

    Object.keys(data).forEach((key) => {
        if (!data[key]) {
            delete data[key];
        }
    });

    const promise = new Promise((resolve, reject) => {
        function handle(data: any) {
            if (data._id !== _id) {
                return;
            }

            cleanup();

            if (data.error) {
                console.error(data);
                reject(new Error(data.error));
            } else {
                resolve(data.data || {});
            }
        }

        let timer = setTimeout(() => {
            cleanup();
            console.error(`Command [${command}] timed-out waiting for the server to respond!`);
            reject("timeout");
        }, 30 * 1000); // 30 seconds

        function cleanup() {
            clearTimeout(timer);
            socket.off(response, handle);
            socket.off("audiobridge-error", handle);
        }

        socket.on(response, handle);
        socket.on("audiobridge-error", handle);
        socket.emit(command, { _id, data });
    });

    return promise;
}

export default {
    connect,
    disconnect,

    on,
    off,
    offAll,

    /**
     * Returns a simple object with connectivity status for the current user.
     * @returns ({ connected, room, muted }) Status object.
     */
    async status() {
        return {
            connected: socket.connected,
            room: state.room,
            muted: state.muted,
        };
    },

    /**
     * List all rooms available to the current user.
     * @return ([{ ... }, ...]) Array of rooms, where each room is an object with details.
     */
    async listRooms() {
        if (!socket.connected) {
            throw new Error("SDK not connected to Rooms server!");
        }
        const data: any = await call("list-rooms", "rooms-list");
        return data.list;
    },

    /**
     * Create a new room.
     * @param description (string) Room name.
     * @param room (string) Room ID (optional, assigned randomly by server if not provided).
     * @param pin (string) Room PIN password (optional).
     * @returns ({ room: number })
     */
    async createRoom(description: string, pin?: string) {
        if (!socket.connected) {
            throw new Error("SDK not connected to Rooms server!");
        }
        return call("create", "created", { description, pin });
    },

    /**
     * Destroy an existing room by ID.
     * @param room (string) Room ID.
     * @returns ({ room: number })
     */
    async destroyRoom(room: number) {
        if (!socket.connected) {
            throw new Error("SDK not connected to Rooms server!");
        }
        return call("destroy", "destroyed", { room: room * 1 });
    },

    /**
     * Join a room by ID.
     * @param room (string) Room ID.
     * @param muted (string) Join with muted microphone.
     * @param pin (string) Room PIN password if required (optional).
     * @param token (string) Room invitation token (if applicable).
     * @param display (string) User display name (optional).
     * @return ({ room, feed, participants })
     */
    async joinRoom(room: number, muted: boolean = false, pin?: string, token?: string, display?: string) {
        if (!socket.connected) {
            throw new Error("SDK not connected to Rooms server!");
        }
        const data: any = await call("join", "joined", { room: room * 1, muted, pin, token, display });
        state.room = data.room;
        state.muted = muted;
        return data;
    },

    /**
     * Leave the current room (if active).
     * @return ({ room, feed })
     */
    async leaveRoom() {
        if (!socket.connected) {
            throw new Error("SDK not connected to Rooms server!");
        }
        if (!state.room) {
            throw new Error("No active room found!");
        }
        const result = await call("leave", "leaving");
        delete state.room;
        delete state.muted;
        return result;
    },

    /**
     * List all participants present in the current room.
     * @return ([{ ... }, ...]) Array of participants, where each participant is an object with details.
     */
    async listParticipants() {
        if (!socket.connected) {
            throw new Error("SDK not connected to Rooms server.");
        }
        if (!state.room) {
            throw new Error("No active room found!");
        }
        const data: any = await call("list-participants", "participants-list", { room: state.room });
        return data.participants;
    },

    /**
     * Mute current user.
     * @return ()
     */
    async mute() {
        if (!socket.connected) {
            throw new Error("SDK not connected to Rooms server.");
        }
        if (!state.room) {
            throw new Error("No active room found!");
        }
        const result = call("mute", "muted");
        state.muted = true;
        return result;
    },

    /**
     * Unmute current user.
     * @return ()
     */
    async unmute() {
        if (!socket.connected) {
            throw new Error("SDK not connected to Rooms server.");
        }
        if (!state.room) {
            throw new Error("No active room found!");
        }
        const result = call("unmute", "unmuted");
        state.muted = false;
        return result;
    },
};
