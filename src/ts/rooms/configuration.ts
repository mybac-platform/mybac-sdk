/**
 * Default WebRTC configuration values.
 * Eventually, we will probably deploy our own STUN/TURN server(s).
 */
export const defaultRTCConfiguration: RTCConfiguration = {
    iceServers: [
        {
            urls: "stun:turn.mybac.ro:3478",
        },
        {
            urls: "turn:turn.mybac.ro:3478",
            username: "mybac",
            credential: "cabym",
        },
    ],
};

/**
 * Configuration options for MyBac Rooms.
 */
export type TRoomsConfiguration = {
    /**
     * Connection URL to the Rooms back-end (Janode).
     * This should be something like "https://rooms.mybac.ro".
     */
    server?: string;

    /**
     * WebRTC configuration options (optional).
     */
    rtc?: RTCConfiguration;
};

export const serviceOptions: TRoomsConfiguration = {
    rtc: defaultRTCConfiguration,
};
