import { io, Socket } from "socket.io-client";
import { serviceOptions, TRoomsConfiguration } from "./configuration";
import { installListeners } from "./events";

export let socket: Socket;

/**
 * Connect the SDk to the MyBac Rooms backe-end service.
 * @param options Service connection options.
 * @returns A Promise which resolves once the service is connected (or rejects on failure).
 */
export async function connect(options: TRoomsConfiguration = {}) {
    if (socket && socket.connected) {
        throw new Error("another connected socket already exists");
    }

    const ioOptions = {
        rejectUnauthorized: false,
        autoConnect: false,
        reconnection: false,
    };

    Object.assign(serviceOptions, options);

    socket = serviceOptions.server ? io(serviceOptions.server, ioOptions) : io(ioOptions);
    if (!socket.connected) {
        socket.connect();
    }

    return new Promise<void>((resolve, reject) => {
        installListeners(socket);
        socket.once("disconnect", reject);
        socket.once("audiobridge-error", reject);
        socket.once("connect", () => {
            socket.offAny(reject);
            resolve();
        });
    });
}

/**
 * Disconnects the SDK(front-end) from the MyBac Rooms back-end service.
 * @returns A Promise which resolves once the connection is disconnected.
 */
export async function disconnect() {
    if (!socket || !socket.connected) {
        return;
    }

    return new Promise((resolve) => {
        socket.on("disconnect", resolve);
        socket.disconnect();
    });
}
