import { makePeerConnection, offers } from "./peers";
import { socket } from "./socket";

function getId() {
    return Math.floor(Number.MAX_SAFE_INTEGER * Math.random());
}

export function kick(feed: string, room: string, secret: string = "adminpwd") {
    const kickData = {
        room,
        feed,
        secret,
    };

    socket.emit("kick", {
        data: kickData,
        _id: getId(),
    });
}

export function trickle(candidate?: RTCIceCandidate | null) {
    const trickleData = candidate ? { candidate } : {};
    const trickleEvent = candidate ? "trickle" : "trickle-complete";

    socket.emit(trickleEvent, {
        data: trickleData,
        _id: getId(),
    });
}

export function configure(jsep: RTCSessionDescriptionInit) {
    const configureData: any = {};
    const configureId = getId();

    configureData.jsep = jsep;
    offers.set(configureId, null);

    socket.emit("configure", {
        data: configureData,
        _id: configureId,
    });
}

/*
export function exists(room: string) {
    const existsData = {
        room,
    };

    socket.emit("exists", {
        data: existsData,
        _id: getId(),
    });
}

// add remove enable disable token mgmt
export function allow(room: string, action: string, token?: string, secret: string = "adminpwd") {
    const allowData: any = {
        room,
        secret,
        action,
    };
    if (action !== "disable" && token) {
        allowData.list = [token];
    }

    socket.emit("allow", {
        data: allowData,
        _id: getId(),
    });
}

export function startForward(room: string, host: string = "localhost", audio_port: number, group?: string, secret: string = "adminpwd") {
    let startData = {
        room,
        host,
        audio_port,
        group,
        secret,
    };

    socket.emit("rtp-fwd-start", {
        data: startData,
        _id: getId(),
    });
}

export function stopForward(room: string, stream: any, secret: string = "adminpwd") {
    let stopData = {
        room,
        stream,
        secret,
    };

    socket.emit("rtp-fwd-stop", {
        data: stopData,
        _id: getId(),
    });
}

export function listForward(room: string, secret: string = "adminpwd") {
    let listData = {
        room,
        secret,
    };

    socket.emit("rtp-fwd-list", {
        data: listData,
        _id: getId(),
    });
}

export async function restartParticipant() {
    const offer = await makePeerConnection();
    configure(offer);
}
*/
