import { closePeerConnection, configurePeerConnection, makePeerConnection, offers } from "./peers";
import { removeAllAudioElements, removeAudioElement } from "./elements";
import { configure } from "./commands";
import { Socket } from "socket.io-client";

/**
 * Manage external events.
 */
export type TEventListener = (...args: any[]) => void;

const listeners = new Map<string, TEventListener[]>();

/**
 * Add a listener from the chain of liseners for a given event name.
 * @param event Event name.
 * @param listener Event listener function.
 */
export function on(event: string, listener: TEventListener) {
    if (!listeners.has(event)) {
        listeners.set(event, []);
    }
    listeners.get(event)!.push(listener);
}

/**
 * Remove a listener from the chain of liseners for a given event name.
 * @param event Event name.
 * @param listener Event listener function.
 */
export function off(event: string, listener: TEventListener) {
    const pool = listeners.get(event);
    if (pool) {
        const index = pool.indexOf(listener);
        if (index >= 0) {
            pool.splice(index, 1);
        }
    }
}

/**
 * Clear all listeners for a given event name.
 * @param event Event name.
 */
export function offAll(event: string) {
    listeners.delete(event);
}

/**
 * Trigger a given event (calls all listeners in the order they were added).
 * @param event Event name.
 * @param args Event parameters.
 */
export function emit(event: string, ...args: any[]) {
    listeners.get(event)?.forEach((listener) => listener.apply(null, args));
}

/**
 * Socket configuration.
 */
export function installListeners(socket: Socket) {
    if (!socket) {
        return;
    }

    socket.on("connect", () => {
        console.log("socket connected");
        socket.sendBuffer = [];
    });

    socket.on("disconnect", () => {
        console.log("socket disconnected");
        offers.clear();
        removeAllAudioElements();
        closePeerConnection();
    });

    socket.on("audiobridge-error", ({ error, _id }) => {
        console.log("audiobridge error", error);
        if (error === "backend-failure" || error === "session-not-available") {
            socket.disconnect();
            return;
        }
        if (offers.has(_id)) {
            offers.delete(_id);
            removeAllAudioElements();
            closePeerConnection();
            return;
        }
    });

    socket.on("joined", async ({ data }) => {
        console.log("you have joined to room", data);
        removeAllAudioElements();
        closePeerConnection();

        try {
            const offer = await makePeerConnection(data.feed);
            configure(offer);
        } catch (error) {
            console.log("error during audiobridge setup/offer", error);
            removeAllAudioElements();
            closePeerConnection();
            return;
        }
    });

    socket.on("kicked", ({ data }) => {
        console.log("you have been kicked out", data);
        closePeerConnection();
        removeAllAudioElements();
        emit("kicked", data);
    });

    socket.on("configured", ({ data, _id }) => {
        console.log("feed configured", data);
        offers.delete(_id);
        configurePeerConnection(data.jsep);
        emit("configured", data);
    });

    socket.on("peer-joined", ({ data }) => {
        console.log("peer joined to room", data);
        emit("peer-joined", data);
    });

    socket.on("peer-kicked", ({ data }) => {
        console.log("participant kicked out", data);
        removeAudioElement(data.feed);
        emit("peer-kicked", data);
    });

    socket.on("peer-configured", ({ data }) => {
        console.log("peer configured", data);
        emit("peer-configured", data);
    });

    socket.on("peer-leaving", ({ data }) => {
        console.log("peer feed leaving", data);
        removeAudioElement(data.feed);
        emit("peer-leaving", data);
    });

    socket.on("talking", ({ data }) => {
        console.log("own talking notify", data);
        emit("talking", data);
    });

    socket.on("peer-talking", ({ data }) => {
        console.log("peer talking notify", data);
        emit("peer-talking", data);
    });

    // socket.on("exists", ({ data }) => {
    //     console.log("room exists", data);
    // });

    socket.on("destroyed", ({ data }) => {
        console.log("room destroyed", data);
        offers.clear();
        // removeAllAudioElements();
        closePeerConnection();
    });

    // socket.on("allowed", ({ data }) => {
    //     console.log("token management", data);
    // });

    // socket.on("rtp-fwd-started", ({ data }) => {
    //     console.log("rtp forwarding started", data);
    // });

    // socket.on("rtp-fwd-stopped", ({ data }) => {
    //     console.log("rtp forwarding stopped", data);
    // });

    // socket.on("rtp-fwd-list", ({ data }) => {
    //     console.log("rtp forwarders list", data);
    // });
}
