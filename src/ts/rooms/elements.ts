const AUDIO_CONTAINER_PREFIX = "__mybac-rooms__audio__";

export function getAudioContainer(): HTMLElement {
    const audioContainerId = AUDIO_CONTAINER_PREFIX;
    let audioContainer = document.querySelector<HTMLElement>("#" + audioContainerId);
    if (!audioContainer) {
        audioContainer = document.createElement("div");
        audioContainer.style.display = "none";
        document.body.appendChild(audioContainer);
    }
    return audioContainer;
}

export function setAudioElement(stream: MediaProvider, feed: string) {
    const audioContainer = getAudioContainer();
    const audioObjectId = AUDIO_CONTAINER_PREFIX + feed;

    let audioObject = audioContainer.querySelector<HTMLAudioElement>("audio#" + audioObjectId);
    if (!audioObject) {
        audioObject = document.createElement("audio");
        audioObject.dataset["feed"] = feed;
        document.body.appendChild(audioObject);
        audioObject.autoplay = true;
        audioObject.srcObject = stream;
    }
}

export function removeAudioElement(feed: string) {
    const audioObjectId = AUDIO_CONTAINER_PREFIX + feed;
    const audioObject = document.querySelector<HTMLAudioElement>("audio#" + audioObjectId);

    if (audioObject) {
        (<MediaStream>audioObject.srcObject).getTracks().forEach((track) => track.stop());
        audioObject.srcObject = null;
        audioObject.remove();
    }
}

export function removeAllAudioElements() {
    const audioContainer = getAudioContainer();
    const audioObjects = Array.from(audioContainer.getElementsByTagName("audio"));

    audioObjects.forEach((audioObject) => {
        (<MediaStream>audioObject.srcObject).getTracks().forEach((track) => track.stop());
        audioObject.srcObject = null;
        audioObject.remove();
    });
}
