import { serviceOptions } from "./configuration";
import { setAudioElement } from "./elements";
import { trickle } from "./commands";

export const offers = new Map();

let audioPeerConnection: RTCPeerConnection | null = null;

export async function makePeerConnection(feed?: string) {
    if (!audioPeerConnection) {
        const pc = new RTCPeerConnection(serviceOptions.rtc);
        audioPeerConnection = pc;

        pc.onnegotiationneeded = (event) => console.log("pc.onnegotiationneeded", event);
        pc.onicecandidate = (event) => trickle(event.candidate);
        pc.oniceconnectionstatechange = () => {
            if (pc.iceConnectionState === "failed" || pc.iceConnectionState === "closed") {
                closePeerConnection(pc);
            }
        };
        pc.ontrack = (event) => {
            console.log("pc.ontrack", event);

            event.track.onunmute = (evt) => {
                console.log("track.onunmute", evt);
                /* TODO set srcObject in this callback */
            };
            event.track.onmute = (evt) => {
                console.log("track.onmute", evt);
            };
            event.track.onended = (evt) => {
                console.log("track.onended", evt);
            };

            const remoteStream = event.streams[0];
            if (feed) {
                setAudioElement(remoteStream, feed);
            }
        };

        const localStream = await navigator.mediaDevices.getUserMedia({ audio: true, video: false });

        console.log("getUserMedia OK");

        localStream.getTracks().forEach((track) => {
            console.log("adding track", track);
            pc.addTrack(track, localStream);
        });
    } else {
        console.log("Performing ICE restart");
        audioPeerConnection.restartIce();
    }

    const offer = await audioPeerConnection.createOffer();
    console.log("create offer OK");
    await audioPeerConnection.setLocalDescription(offer);
    console.log("set local sdp OK");
    return offer;
}

export function configurePeerConnection(jsep: RTCSessionDescriptionInit | null) {
    if (audioPeerConnection && jsep) {
        audioPeerConnection
            .setRemoteDescription(jsep)
            .then(() => console.log("remote sdp OK"))
            .catch((e) => console.log("error setting remote sdp", e));
    }
}

export function closePeerConnection(pc: RTCPeerConnection | null = audioPeerConnection) {
    if (!pc) {
        return;
    }

    pc.getSenders().forEach((sender) => {
        if (sender.track) {
            sender.track.stop();
        }
    });

    pc.getReceivers().forEach((receiver) => {
        if (receiver.track) {
            receiver.track.stop();
        }
    });

    pc.onnegotiationneeded = null;
    pc.onicecandidate = null;
    pc.oniceconnectionstatechange = null;
    pc.ontrack = null;
    pc.close();

    if (pc === audioPeerConnection) {
        audioPeerConnection = null;
    }
}
