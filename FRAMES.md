# MyBac Content Frames

## Introduction

MyBac content is organized in _Courses_, _Campaigns_, _Missions_. A _Mission_ is an ordered succession of **_Frames_** where each _Frame_ is one single step to be displayed to the user and played.

### Frame Structure

In the database each `Frame` must contain (at least) these fields (remember that not all are returned by the API):

```js
{
    "id": "...",

    // Frame type.
    "type": "exposition.article",

    // Main Content Block.
    "cover": { },

    // Additional content or data which can't be part of the `cover` block.
    "options": { },

    // Contains the correct answer (if any).
    "solution": { },

}
```

### Solving a Frame

Solving a _Frame_ requires you to call `/frame/{id}/solve` with a JSON payload that is different depending on the `frame.type` field. Below, you will also find the JSON schema expected by this endpoint for each frame type (i.e. the `.../solve.json` files).

### Other Content Blocks

In addition to the fields above, a _Frame_ can optionally have other Content Blocks associated with it, such as an **_explanation_** (a single Content Block) or **_hints_** (zero or more Content Blocks).

These blocks are not part of the _Frame_ object structure and are obtained via dedicated API endpoints like `/frame/{id}/explanation` and `/frame/{id}/hints` (see the API for the specs).

### JSON-Schema Validation Files

This repository provides two JSON-Schema files for each _Frame_ type:

1. `structure.json` with information about how the frame must be structured in the database; this schema is used to validate each Frame prior to being saved in the database (during editing);
2. `solve.json` detailing the expected request body when solving the frame using the `/frame/{id}/solve` API endpoint;

Both these files are under path `./src/schemas/frames` for each frame type (e.g. `./src/schemas/frames/exposition.article/structure.json`).

You can also get these schema files online from any MyBac API deployment, for example:

-   https://alpha-api.mybac.ro/schemas/frames/exposition.article/structure.json;
-   https://alpha-api.mybac.ro/schemas/frames/exposition.article/solve.json;

## Exposition Frames

These are presentation frames (i.e. "expositions") that are non-interactive and are designed to show theory material to the user for learning. They don't need any solution when solved (the player just presses "Next").

### HTML Article:

-   Theory frame showing an HTML article.
    -   `frame.type: "exposition.article"`
    -   `frame.cover`: a single Content Block of `article` type;
    -   `frame.options`: empty object.
    -   `frame.solution`: empty object.
-   Solved with:
    -   Empty object.

### Video clip:

-   Theory frame showing a single video clip.
    -   `frame.type: "exposition.video"`
    -   `frame.cover`: a single Content Block of `video` type;
    -   `frame.options`: empty object.
    -   `frame.solution`: empty object.
-   Solved with:
    -   Empty object.

## Challenge frames

These frames are designed to challenge the players, testing their knowledge & skills. The user is expected to provide the correct solutions to solve these frames.

### Multiple-Choice

-   Multiple-choice question, with either a single correct answer or many.
    -   `frame.type: "challenge.multiple-choice"`
    -   `frame.cover`: a single Content Block of `article` type, i.e. the question part;
    -   `frame.options`:
        -   `frame.options.choices`: array of `article` Content Blocks of available choices;
        -   `frame.options.multiple`: if true, the user may select multiple options;
        -   `frame.options.shuffle`: if true, the order should be shuffled when displayed;
    -   `frame.solution`: array with the indexes of the correct choices (above).
-   Solved with:
    -   array with indexes of choices selected by user.
